import Head from 'next/head';
import React from 'react';
import data from '../../../public/landingpage-assicurazione-auto.json'

const Landing = (props) => {
    // let landingParameter
    // let searchParameter
    // React.useEffect(() => {
    //     const searchParams = new URLSearchParams(window.location.search)
    //     landingParameter = searchParams.get('landing')
    //     searchParameter = searchParams.get('search')
    // })
    return (
        <div>
            <Head>
                <meta name="title" content={props.landingType} />
                <meta name="customForTest" content={props.generatedTime} />
                <title>{props.qads ? props.qads : props.landingType}</title>
                <meta name="prova" content={props.pageTextReplaced}/>
            </Head>
            <div>
                <p>Landingpage for {props.qads ? props.qads : props.landingType}</p>
                <p>generated on: {props.generatedTime}</p>
                {/* <p>Query param landing: {landingParameter}</p>
                <p>Query param search: {searchParameter}</p> */}
                <br/><br/>
                <p>Content from json: {props.pageText}</p>
                <p>Content updated on server: {props.pageTextReplaced && props.pageTextReplaced.length ? props.pageTextReplaced : props.pageText}</p>
            </div>
        </div>
    );
};

export async function getStaticPaths() {
    return {
        paths: [{ params: { vehicle: ["auto"] } }, { params: { vehicle: ["moto"] } }],
        fallback: true, //false or 'blocking' gives a 404 when it is statically requested a page different from the one in paths array, while true makes it generated anyway 
    };
}

export async function getStaticProps({ params }) {
    console.log('getStaticProps of Landing page for vehicle:', params.vehicle)
    const vehicleType = params.vehicle[0]
    let vehicleQads = null //old qads parameter
    if (params.vehicle.length > 1) {
        vehicleQads = params.vehicle[1]
    }
    const pageText = data.components[0].text
    let pageTextReplaced = ''
    if (vehicleQads) {
        pageTextReplaced = pageText.replace('${din_landing_key}', vehicleQads)
    }
    return {
        props: {
            landingType: vehicleType,
            qads: vehicleQads,
            generatedTime: new Date().toString(),
            pageText,
            pageTextReplaced,
        },
        revalidate: 6
    };
}

export default Landing;
