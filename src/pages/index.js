import React from 'react';

const HomePage = ({ day }) => {
    return (
        <div>
            <p>Today is: {day}</p>
        </div>
    );
};

export async function getStaticProps() {
    const today = new Date()

    return {
        props: {
            day: today.toString(),
        },
        revalidate: 10
    };
}

export default HomePage;